use reqwest::header;

use crate::model::SbankenConfig;

pub fn build_authorization_http_client(config: &SbankenConfig) -> reqwest::blocking::Client {
    debug!("Building HTTP Client for authorization");

    let auth_header = generate_auth_header(
        base64_encode_uname_pw(config.username.clone(), config.password.clone()),
        AuthenticationType::Basic,
    );

    let mut headers = header::HeaderMap::new();
    headers.insert(header::AUTHORIZATION, auth_header);

    return match reqwest::blocking::Client::builder()
        .default_headers(headers)
        .build()
    {
        Ok(client) => client,
        Err(error) => {
            error!("Building HTTP Client for authorization failed: {}", error);
            panic!("building client failed");
        }
    };
}

pub fn build_api_client(credentials: String) -> reqwest::blocking::Client {
    debug!("Building HTTP Client for authorized API requests");

    let auth_header = generate_auth_header(credentials, AuthenticationType::Bearer);

    let mut headers = header::HeaderMap::new();
    headers.insert(header::AUTHORIZATION, auth_header);

    return match reqwest::blocking::Client::builder()
        .default_headers(headers)
        .build()
    {
        Ok(client) => client,
        Err(error) => {
            error!("Building HTTP Client failed: {}", error);
            panic!("building client failed");
        }
    };
}
// fooo
//
//
extern crate base64;
extern crate urlencoding;

use base64::encode;
use urlencoding::encode as urlencode;
//use reqwest::header;

pub enum AuthenticationType {
    Basic,
    Bearer,
}

pub fn base64_encode_uname_pw(username: String, password: String) -> String {
    return encode(&format!(
        "{}:{}",
        urlencode(&username),
        urlencode(&password)
    ));
}

pub fn generate_auth_header(
    credentials: String,
    auth_type: AuthenticationType,
) -> header::HeaderValue {
    let auth_header = match auth_type {
        AuthenticationType::Basic => format!("Basic {}", credentials),
        AuthenticationType::Bearer => format!("Bearer {}", credentials),
    };

    generate_header_value(&auth_header)
}

pub fn generate_header_value(value: &str) -> header::HeaderValue {
    return match header::HeaderValue::from_str(value) {
        Ok(val) => val,
        Err(error) => {
            panic!("Failed to generate HTTP header: {}", error);
        }
    };
}
